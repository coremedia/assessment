<?php
  include "../libs/php/mysql.php";

  if (isset($_POST)) { 
    $mysql = new Connection();
    $sql = "SELECT count(id), user_type FROM user WHERE email = '" .$_POST['email']. "' and password = '" . $_POST['password']. "';";
    $loginResult = $mysql->getValues($sql);
    $loginRow = mysqli_fetch_assoc($loginResult);
    $userType = $loginRow['user_type'];
    if(isset($_POST['user_login']) && $userType !== 'user') {
      die("Error: your user is not of type Normal User, have you tried the Admin Login?");
    } else if (isset($_POST['admin_login']) && $userType !== 'admin') {
      die("Error: your user is not of type Normal User, have you tried the Admin Login?");
    }

    $mysqlBrands = new Connection();
    $brandsSQL = "select * from `assessment`.`brand`";
    $brandsResult = $mysqlBrands->getValues($brandsSQL);

    $mysqlProducts = new Connection();
    $productsSQL = "SELECT brand.name as bname, brand.id as bId, products.`name` as pname, products.details FROM products JOIN brand ON products.brand_id = brand.id ORDER BY bname";
    $productsResult = $mysqlProducts->getValues($productsSQL);
  } 
?> 

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question 5</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

    <?php
      if($userType === 'admin') {
    ?>

          <div class="container">

            <form data-toggle="validator" role="form" action="add_brand.php" method="post">
              <h2 class="form-signin-heading">Add new brand</h2>
              <label for="inputEmail" class="sr-only">Brand name</label>
              <input type="text" id="brand" name="name" class="form-control" required autofocus>
              <button class="btn btn-lg btn-primary btn-block" type="submit" name="add_brand">Add brand</button>
            </form>

          </div> <!-- /container -->
          <div class="container">

            <form data-toggle="validator" role="form" action="add_product.php" method="post">
              <h2 class="form-signin-heading">Add New Product</h2>
                <div class="form-group">
                  <label for="inputName" class="control-label">Brand</label>
                  <select form-control name="brand_id">
                    <?php 
                      if (isset($brandsResult) && mysqli_num_rows($brandsResult) > 0) {
                        while($row = mysqli_fetch_assoc($brandsResult)) {
                      ?>
                      <option name="brand_id" value="<?=$row['id']?>"><?=$row['name']?></option>
                    <?php 
                        }
                      }
                    ?>
                  </select>
                </div>
                <div class="form-group">
                  <label for="product" class="control-label col-sm-2">Product Name</label>
                  <input type="text" id="name" name="name" class="form-control" required>
                </div>
                <div class="form-group">
                  <label for="product" class="control-label col-sm-2">Product Details</label>
                  <input type="text" id="details" name="details" class="form-control" required>
                </div>
              <button class="btn btn-lg btn-primary btn-block" type="submit" name="add_product">Add product</button>
            </form>

          </div> <!-- /container -->
    <?php 
          }
          else {
            echo "<div class=\"form_container\">";
          if (isset($productsResult) && mysqli_num_rows($productsResult) > 0) {
            $prevBrandName = 0;
            while($row = mysqli_fetch_assoc($productsResult)) {
              if ($row['bId'] !== $prevBrandName) {
                echo "<h4>Brand: ".$row['bname']." <br><br></h4>";
              }
              echo "Product name: " .$row['pname']." <br>";
               
              echo "Details: ".$row['details']." <br><br><br><br>";
              $prevBrandName = $row['bId'];                      
            }  
            echo "</div>";
        }

      } 


    ?>
    </div>
  <script src="../libs/js/jquery-3.2.0.min.js"></script>
  <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>
