<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question 5</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
      <div class="row">
        <div class="col col-md-4">
          <form class="form-signup" action="register.php" method="post">
            <h2 class="form-signin-heading">User Sign-up</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
            <div class="form-group">
              <label for="inputName" class="control-label">User type</label>
              <select form-control name="user_type">
                <option value="admin">Administrator</option>
                <option value="user">Normal User</option>
              </select>
            </div>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="user_register">Sign up</button>
          </form>
          <span>Write any email and password so you can create a new user.</span>
        </div>


        <div class="col col-md-3">
          <form class="form-signin" action="login.php" method="post">
            <h2 class="form-signin-heading">User Log-in</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="user_login">Sign in</button>
          </form>
          <span>Use user:john@test.com and password: password to log in as a normal user</span>
        </div>
        <div class="col col-md-3">
          <form class="form-signin" action="login.php" method="post">
            <h2 class="form-signin-heading">Admin Log-in</h2>
            <label for="inputEmail" class="sr-only">Email address</label>
            <input type="email" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
            <label for="inputPassword" class="sr-only">Password</label>
            <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="admin_login">Sign in</button>
          </form>
          <span>Use user: luis@test.com and password: password to log in as an Admin user</span>
        </div>
      </div>
    </div>
  <script src="../libs/js/jquery-3.2.0.min.js"></script>
  <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>
