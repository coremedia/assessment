<?php
  include "../libs/php/mysql.php";

  if (isset($_POST['add'])) { 
    $mysql = new Connection();
    $sqlInsert = "insert into `assessment`.`address_book` ( `email`, `phone`, `name`, `address`) values ( '" . $_POST['email'] . "', '" . $_POST['phone'] . "', '" . $_POST['name'] . "', '" . $_POST['address'] . "')";
    $mysql->insertValues($sqlInsert);
    $sql = "SELECT id, name, address, email, phone FROM address_book;";
    $addreBookResult = $mysql->getValues($sql);
  } else if (isset($_POST['update'])) {
    $mysql = new Connection();
    $sqlUpdate = "update `assessment`.`address_book` set `name`='" . $_POST['name'] . "', `address`='" . $_POST['address'] . "', `email`='" . $_POST['email'] . "', `phone`='" . $_POST['phone'] . "' where `id`='" . $_POST['id'] . "'; ";
    $mysql->insertValues($sqlUpdate);
    $sql = "SELECT id, name, address, email, phone FROM address_book;";
    $addreBookResult = $mysql->getValues($sql);
  } else if (isset($_POST['delete'])) { 
    $mysql = new Connection();
    $sqlDelete = "DELETE FROM `assessment`.`address_book` WHERE `id`='" . $_POST['id'] . "';";
        $mysql->insertValues($sqlDelete);
    $sql = "SELECT id, name, address, email, phone FROM address_book;";
    $addreBookResult = $mysql->getValues($sql);
  } else {
    $mysql = new Connection();
    $sql = "SELECT id, name, address, email, phone FROM address_book;";
    $addreBookResult = $mysql->getValues($sql);
  }
?> 

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question 3</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <div class="container">
          <div class="row">  
            <div class="col-md-12">
            <h4>Address Book</h4>
            <div class="table-responsive">      
              <table id="mytable" class="table table-bordred table-striped">
                <thead>
                  <th>First Name</th>
                  <th>Address</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </thead>
                <tbody>
                <?php
                  if (isset($addreBookResult) && mysqli_num_rows($addreBookResult) > 0) {
                    while($row = mysqli_fetch_assoc($addreBookResult)) {
                  ?>
                  <tr>
                    <td><?=$row['name'] ?></td>
                    <td><?=$row['address'] ?></td>
                    <td><?=$row['email'] ?></td>
                    <td><?=$row['phone'] ?></td>
                    <td>
                      <p data-placement="top" data-toggle="tooltip" title="Edit">
                        <button class="btn btn-primary btn-xs" data-title="Edit" data-toggle="modal" data-target="#edit_<?=$row['id']?>" >
                          <span class="glyphicon glyphicon-pencil"></span>
                        </button>
                      </p>
                    </td>
                    <td>
                      <p data-placement="top" data-toggle="tooltip" title="Delete">
                        <button class="btn btn-danger btn-xs" data-title="Delete" data-toggle="modal" data-target="#delete_<?=$row['id']?>" >
                          <span class="glyphicon glyphicon-trash"></span>
                        </button>
                      </p>
                    </td>
                  </tr>
                  <?php }
                  } ?>
                </tbody>  
              </table>
            </div>
          </div>
        </div>
        <form role="form" class="form-inline" action="<?=$_SERVER['PHP_SELF']?>" method="post">
          <div class="form-group">
            <label for="name">Name:</label>
            <input type="name" class="form-control" id="name" name="name" required>
          </div>
          <div class="form-group">
            <label for="pwd">Address:</label>
            <input type="text" class="form-control" id="address" name="address" required>
          </div>
          <div class="form-group">
            <label for="email" class="control-label">Email:</label>
            <input type="email" class="form-control" id="email" name="email" required>
          </div>
          <div class="form-group">
            <label for="phone" class="control-label">Phone Number:</label>
            <input type="tel" class="form-control" id="phone" name="phone" required>
          </div>
          <button type="submit" name="add" class="btn btn-primary">Add new</button>
        </form>
      </div>

      <?php 
        mysqli_data_seek($addreBookResult, 0);
        if (isset($addreBookResult) && mysqli_num_rows($addreBookResult) > 0) {
          while($row = mysqli_fetch_assoc($addreBookResult)) {
      ?>
      <form role="form" name="edit" class="form" action="<?=$_SERVER['PHP_SELF']?>" method="post">
      <input type="hidden" name="id" value="<?=$row['id']?>">
      <div class="modal fade" id="edit_<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
              <h4 class="modal-title custom_align" id="Heading">Edit Your Detail</h4>
            </div>
              <div class="modal-body">
              <div class="form-group">
                <input class="form-control " name="name" type="text" value="<?=$row['name'] ?>">
              </div>
              <div class="form-group">
                <input class="form-control " name="address" type="text" value="<?=$row['address'] ?>">
              </div>
              <div class="form-group">
                <input class="form-control " name="email" type="text" value="<?=$row['email'] ?>">
              </div>
              <div class="form-group">
                <input class="form-control " name="phone" type="text" value="<?=$row['phone'] ?>">
              </div>
            </div>
            <div class="modal-footer ">
              <button name="update" type="submit" class="btn btn-warning btn-lg" style="width: 100%;"><span class="glyphicon glyphicon-ok-sign"></span> Update</button>
            </div>
          </div>
          <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
      </div>
      </form>
      
      <form role="form" name="delete" class="form-inline" action="<?=$_SERVER['PHP_SELF']?>" method="post">
      <div class="modal fade" id="delete_<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <input type="hidden" name="id" value="<?=$row['id']?>">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
              <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">
              <div class="alert alert-danger">
                <span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?
              </div> 
            </div>
            <div class="modal-footer ">
              <button name="delete" type="submit" class="btn btn-success" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
              <button type="button" class="btn btn-default" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
          </div>
          <!-- /.modal-content --> 
        </div>
        <!-- /.modal-dialog --> 
      </div>
      <?php 
        }
      }
      ?>
      </form>
    <!-- </form> -->
    <script src="../libs/js/jquery-3.2.0.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>

  </body>
</html>
