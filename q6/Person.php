<?php
include "Account.php";
class Person {
	
	var $name;
	var $address;
	var $phone;
	var $email;
	var $Account;

	function __construct($name, $address, $phone, $email, $accountType) {
		$this->name = $name;
		$this->address = $address;
		$this->phone = $phone;
		$this->email = $email;
		$this->Account = new Account ($accountType);
	}

}

?>