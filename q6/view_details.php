<?php
  include "Person.php";
  // let's start a session
  session_start();

  if (isset($_POST) && isset($_SESSION["customer"]) && isset($_POST["withdraw"])) {
    $customer = $_SESSION["customer"];
    if ($customer->Account->withdrawMoney($_POST["withdraw_ammount"]) === false) {
      $alert = "The overdraft facility is not available.";
    }
  } else if (isset($_POST) && isset($_SESSION["customer"]) && isset($_POST["deposit"])) {
    $customer = $_SESSION["customer"];
    $customer->Account->depositMoney($_POST["deposit_ammount"]);
  }
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>View details</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="form_container">  
      <div class="row">  
        <?php
          if (!isset($_SESSION["customer"])) {
        ?>
          <a href="create_account.php" class="btn btn-info" role="button">Create Account</a>
        <?php 
          } else {
            $customer = $_SESSION["customer"];
            if (isset($alert)) {
              echo '<span style="color:red">' . $alert . '</span>';
            }
        ?>
            <h2>Customer Details</h2>
            <table class="table table-bordered">
              <tbody>
                <tr>
                  <td>Name</td>
                  <td><?=$customer->name?></td>
                </tr>
                <tr>
                  <td>Address</td>
                  <td><?=$customer->address?></td>
                </tr>
                <tr>
                  <td>Phone</td>
                  <td><?=$customer->phone?></td>
                </tr>
                <tr>
                  <td>Email</td>
                  <td><?=$customer->email?></td>
                </tr>
                <tr>
                  <td>Account Number</td>
                  <td><?=$customer->Account->accountNumber?></td>
                </tr>
                <tr>
                  <td>Account Type</td>
                  <td><?=$customer->Account->accountType?></td>
                </tr>
                <tr>
                  <td>Money</td>
                  <td>$<?=$customer->Account->money?></td>
                </tr>
              </tbody>
            </table>
          </div>
          <div class="container">
          <a href="view_details.php" class="btn btn-info" role="button">View account details</a>
          <a href="withdraw.php" class="btn btn-info" role="button">Withdraw money</a>
          <a href="deposit.php" class="btn btn-info" role="button">Deposit money</a>
          <a href="exit.php" class="btn btn-info" role="button">Exit</a></div>

        <?php 
          }
        ?>
    </div>
    <script src="../libs/js/jquery-3.2.0.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>

