<?php
  include "Person.php";
  // let's start a session
  session_start(); 

  if (isset($_POST)) {
    // create a Person object
    $person = new Person ($_POST["name"], $_POST["address"], $_POST["phoneNumber"], $_POST["email"], $_POST["account_type"]);
    // add the person object to our session on a "customer" index.
    $_SESSION["customer"] = $person;
  }
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Created account</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="form_container">  
      <div class="row">  
        <?php
          if (!isset($_SESSION["customer"])) {
        ?>
          <a href="create_account.php" class="btn btn-info" role="button">Create Account</a>
        <?php 
          } else {
            $customer = $_SESSION["customer"];
        ?>
          <div class="container"><h4>You are currently on the <?=$customer->name?>'s account</h4>
          <a href="view_details.php" class="btn btn-info" role="button">View account details</a>
          <a href="withdraw.php" class="btn btn-info" role="button">Withdraw money</a>
          <a href="deposit.php" class="btn btn-info" role="button">Deposit money</a>
          <a href="exit.php" class="btn btn-info" role="button">Exit</a></div>
          
        <?php 
          }
        ?>
      </div>
    </div>
    <script src="../libs/js/jquery-3.2.0.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>

