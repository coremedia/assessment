<?php
  include "Person.php";
 // let's start a session
 session_start(); 
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Create new account</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
        <?php
          if (isset($_SESSION["customer"])) {
        ?>
       	<div class="form_container">  
     	 <div class="row">  
          <a href="view_details.php" class="btn btn-info" role="button">View account details</a>
          <a href="withdraw.php" class="btn btn-info" role="button">Withdraw money</a>
          <a href="deposit.php" class="btn btn-info" role="button">Deposit money</a>
          <a href="exit.php" class="btn btn-info" role="button">Exit</a></div>
         </div>
        </div>
        <?php 
          } else {
        ?>
    <div class="form_container">
      <form data-toggle="validator" role="form" action="create_account_result.php" method="post">
        <div class="form-group">
          <label for="inputName" class="control-label">Name</label>
          <input type="text" class="form-control" id="name" name="name" required>
        </div>
        <div class="form-group">
          <label for="inputName" class="control-label">Address</label>
          <input type="text" class="form-control" id="address" name="address" required>
        </div>
        <div class="form-group">
          <label for="inputName" class="control-label">Phone Number</label>
          <input type="tel" class="form-control" id="phoneNumber" name="phoneNumber" required>
        </div>
        <div class="form-group">
          <label for="inputName" class="control-label">Email</label>
          <input type="email" class="form-control" id="email" name="email" required>
        </div>
        <div class="form-group">
          <label for="inputName" class="control-label">Account Type</label>
          <select form-control name="account_type" required>
              <option value="premium">Premium</option>
              <option value="basic">Basic</option>
          </select>
        </div>
        <div class="form-group">
          <button type="submit" name="create_account" class="btn btn-primary">Create</button>
        </div>
      </form>
    </div>
            <?php 
          }
        ?>
    <script src="../libs/js/jquery-3.2.0.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>

