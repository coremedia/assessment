<?php
  session_start(); 
  // let's destroy the session
  session_unset();
  session_destroy();
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question 1</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="form_container">  
      <div class="row">  
        <?php
          if (!isset($_SESSION["customer"])) {
        ?>
          <a href="create_account.php" class="btn btn-info" role="button">Create Account</a>
        <?php 
          } else {
            $customer = $_SESSION["customer"];
        ?>
          <div class="container"><h1> You are currently on the <?=$customer->name?>'s account</h1></div>
          <a href="operate.php" class="btn btn-info" role="button">Operate Account</a>
          <a href="exit.php" class="btn btn-info" role="button">Exit</a>
        <?php 
          }
        ?>
      </div>
    </div>
    <script src="../libs/js/jquery-3.2.0.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>

