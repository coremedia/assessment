<?php

class account {
	
	var $accountNumber;
	var $accountType;
	var $money;

	function __construct($accountType) {
		$this->accountNumber = mt_rand();
		$this->accountType = $accountType;
		$this->money = 0;
	}

	function getAccountNumber (){
		return $this->accountNumber;
	}

	function getMoney () {
		return $this->money;
	}

	function depositMoney ($ammount) {
		$this->money += $ammount;
	}

	function withdrawMoney ($ammount) {
		// let's verify that the person has enough money
		// if person is withdrawing more money than the available on his account, we won't allow the transaction
		if (($this->money - $ammount) < 0) {
			return false;
		} else { 
			return $this->money -= $ammount;
		}
		
	}

}

?>