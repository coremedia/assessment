<?php
    // Let's populate a couple of arrays with some values for weather and season.
    // also, we'll write these values with Lower and Upper case, so we will print them all
    // as upper case later (this is one of the requirements of the task)
    $weatherArray = array("sunshine","clouds","rain","hail","sleet","snow","wind","cold","Heat");
    $seasonArray = array("spring","Summer","autumn","Winter");

    function createChecklist($arrayList, $type) {
      $string = '';
      foreach ($arrayList as $value) {
          // we make sure that the printed value is going to be Uppercased on the first letter.
          $uppercaseValue = ucfirst($value);
          $string  .= '<div class="checkbox">'
                . ' <input type="checkbox" name="' . $type . '_' . $value . '" value="' . $value . '">' . $uppercaseValue .'</label>'
                . '</div>';
      }
      return $string;
    }

    function getValues ($type) {
      $typeString = '<ul>';
      foreach ($_POST as $key => $value) {
        if (strpos($key, $type) !== false) {
           $typeString .= '<li>' . $value . '</li>';
        }
      }
      return $typeString .= '</ul>';
    }


?> 

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question 2</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <form role="form" action="<?=$_SERVER['PHP_SELF']?>" method="post">
      <h1>Your Favourite Seasons and Weather are:</h1>
      <?php
        if (isset($_POST['submit'])) { ?>

          <h3>Your favourite weather in <?=$_POST['city']?> is:</h3>
          <?=ucfirst(getValues('weather'))?>
          <h3>Your favourite weather in <?=$_POST['city']?> is:</h3>
          <?=ucfirst(getValues('season'))?>

      <?php
        } else {
      ?>
      <div class="form_container">
        <h2>Please enter the city of your choice:</h2>
        <label for="City">City: </label>
        <input type="text" name="city">
        <p>Please choose the kinds of seasons and weather you like from the list below</p>
        <p>Choose all that apply.</p>
      </div>
      <div class="form_container">
        <label>Weather</label>
        <?php
          echo createCheckList ($weatherArray, 'weather');
        ?>
      </div>
      <div class="form_container">
        <label>Season</label>
        <?php
          echo createCheckList ($seasonArray, 'season');
        ?>
        <button name="submit" type="submit" class="btn btn-primary">Submit</button>
      </div>
      <script src="../libs/js/jquery-3.2.0.min.js"></script>
      <script src="../libs/js/bootstrap.min.js"></script>
    </form>
    <?php 
    }
    ?>
  </body>
</html>
