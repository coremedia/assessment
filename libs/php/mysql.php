<?php
class Connection {
	// Change these according to your mysql configuration
	var $servername = "127.0.0.1";
	var $username = "root";
	var $password = "root";
	var $dbname = "assessment";
	var $port = "8889";
	var $conn;
	var $last_inserted_id;

	// constructor to create connection to mysql
	function __construct () {
		$conn = new mysqli($this->servername, $this->username, $this->password, $this->dbname, $this->port);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
		$this->conn = $conn;
	}
	
	function insertValues ($sql) {
		if ($this->conn->query($sql) === TRUE) {
			$this->last_inserted_id = $this->conn->insert_id;
		    return true;
		} else {
		    //echo "Error: " . $sql . "<br>" . $conn->error;
		    return false;
		}
		$conn->close();
	}

	function getValues ($sql) {
		$result = $this->conn->query($sql);

		if ($result->num_rows > 0) {
		    return $result;
		} else {
			return false;
		}
		$conn->close();
	}
}

?>