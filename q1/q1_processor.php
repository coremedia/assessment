<?php
	// lets process the form's variables
    // [lastName] 
	// [firstName]
	// [address] 
	// [city]
	// [state]
	// [zip] => 0629 
	// [country] 
	// [phoneNumber] 
	// [email]
	// [birthDate]
	// [gender]
	// [session]
	// [comments]

	// open a file with w+ which means write and read permission
	$filew = fopen("q1.txt", "w") or die("Unable to open file, check your folder's permission");
	
	// let's get the values of the form and write them on a file
	foreach ($_POST as $key => $value) {
		$string = $key . ": " . $value . "\n";
		fwrite ($filew, $string);
	}
	fclose($filew);
	
	$filer = fopen("q1.txt", "r") or die("Unable to open file, check your folder's permission");

	// let's read the file data that we just saved:
    while (!feof($filer)) {
    	$buffer = fgets($filer, 512);
    	echo ucfirst($buffer) . "<br>";
    }

	// close the file
	fclose($filer);
?>