<?php
  include "../libs/php/mysql.php";

  if (isset($_POST['submit']) && (!empty($_POST['tires']) || !empty($_POST['shipping_address']) || !empty($_POST['spark_plugs']))) { 
    $mysql = new Connection();
    $sqlInsert = "insert into `assessment`.`orders` ( `spark_plugs`, `shipping_address`, `tires`, `oil`) values ( '" . $_POST['spark_plugs'] . "', '" . $_POST['shipping_address'] . "','" . $_POST['tires'] . "', '" . $_POST['oil'] . "');";
    $mysql->insertValues($sqlInsert);
    $sql = "SELECT *, (SUM(tires)+SUM(oil)+SUM(spark_plugs)) as total_items FROM orders where id = '" . $mysql->last_inserted_id . "';";

    $orders_result = $mysql->getValues($sql);
  }
?> 

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Question 4</title>
    <!-- Bootstrap -->
    <link href="../libs/css/bootstrap.min.css" rel="stylesheet">
    <link href="../libs/css/assessment.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

  <?php if (!isset($_POST['submit'])) { ?>
      <div class="container">
        <form role="form" class="form-horizontal" action="<?=$_SERVER['PHP_SELF']?>" method="post">
          <div class="form-group">
            <label for="name" class="control-label col-sm-3">Fill your order:</label>
          </div>
          <div class="form-group">
            <label for="name" class="control-label col-sm-3">Tires:</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="tires" name="tires">
            </div>
          </div>
          <div class="form-group">
            <label for="pwd" class="control-label col-sm-3">Oil:</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="oil" name="oil">
            </div>
          </div>
          <div class="form-group">
            <label for="email" class="control-label col-sm-3">Spark Plugs:</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="spark_plugs" name="spark_plugs">
            </div>
          </div>
          <div class="form-group">
            <label for="phone" class="control-label col-sm-3">Shipping Address:</label>
            <div class="col-sm-3">
              <input type="text" class="form-control" id="shipping_address" name="shipping_address" required>
            </div>
          </div>
          <button type="submit" name="submit" class="btn btn-primary col-sm-4">Order</button>
        </form>
      </div>


    <?php } else {
        echo "<div class=\"form_container\">";
        if (isset($orders_result) && mysqli_num_rows($orders_result) > 0) {
          $priceTires = 98;
          $priceOil = 15;
          $priceSparks = 8;
          while($row = mysqli_fetch_assoc($orders_result)) {
            if ($row['total_items'] > 0) {
              echo "<h4>Order Results</h4><br>";
              echo "Order processed at " . date("Y-m-d H:i:s") . "<br>";
              if ($row['tires'] > 0) { 
                echo $row['tires'] . ' Tires <br>'; 
                $totalTires = $row['tires'] * $priceTires;
              }
              if ($row['oil'] > 0) {
               echo $row['oil'] . ' Bottles of oil <br>'; 
               $totalOil = $row['oil'] * $priceOil;
              }
              if ($row['spark_plugs'] > 0) { 
                echo $row['spark_plugs'] . ' Spark Plugs <br>';
                $totalSparks = $row['spark_plugs'] * $priceSparks; 
              }
              echo "<b>Items ordered:</b> " . $row['total_items'] ."<br>";
              $total = $totalTires + $totalOil + $totalSparks;
              echo "<b>Sub-total:</b> $" . $total . "<br>";
              echo "<b>GST 15%: </b> $" . $total * 0.15 . "<br>";
              echo "<b>Total including Tax: </b> $" . (($total * 0.15) + $total) . "<br><br>";
              echo "<br><b>Address to ship: </b>" . $row['shipping_address'] . "<br>";
              echo "<br>Order written.<br>";

            } else {
              echo "You did not order anything from the previous page!";
            }
          }
        }
        echo "</div>"; 
      }
    ?>
    <script src="../libs/js/jquery-3.2.0.min.js"></script>
    <script src="../libs/js/bootstrap.min.js"></script>
  </body>
</html>
